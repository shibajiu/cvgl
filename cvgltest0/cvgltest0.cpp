// cvgltest0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//opencv
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
//sdl2
#include <SDL.h>
#include <SDL_main.h>
//gl
#include <GL/glew.h>
#include <gl2tools.h>

#define var auto

using namespace std;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
SDL_Window *mainwindow; /* Our window handle */
SDL_GLContext maincontext; /* Our opengl context handle */

int sdglInit();
void checkSDLError(int line = -1);

int main(int argc,char* argv[])
{
	cv::VideoCapture capture(0);
	if (!capture.isOpened()) {
		cout << "capture failed" << endl;
		system("pause");
		return -1;
	}

	SDL_Event event;
	if (sdglInit()) {
		cout << "sdglInit failed" << endl;
		system("pause");
		return 1;
	}

	cv::Mat frame;
	capture >> frame;
	GLenum inputColourFormat = GL_BGR;
	if (frame.channels() == 1)
	{
		inputColourFormat = GL_LUMINANCE;
	}
	GLuint textid;
	GLint textwidth(frame.cols), textheight(frame.rows);
	glGenTextures(1, &textid);
	glBindTexture(GL_TEXTURE_2D, textid);
	glTexImage2D(
		GL_TEXTURE_2D, 
		0, GL_RGB, 
		textwidth,
		textheight,
		0, 
		inputColourFormat, 
		GL_UNSIGNED_BYTE, 
		nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnable(GL_TEXTURE_2D);
	glClearColor(0, 0, 0, 0);

	bool isStop = false;
	while (!isStop) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT:
				isStop = true;
				break;
			default:
				break;
			}
		}
		capture >> frame;
		cv::Mat flipmat;
		cv::flip(frame, flipmat, 0);
		glClear(GL_COLOR_BUFFER_BIT);
		/*glLoadIdentity();
		glBindTexture(GL_TEXTURE_2D, textid);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textwidth, textheight, GL_BGR, GL_UNSIGNED_BYTE, frame.ptr());*/
		glDrawPixels(textwidth, textheight, GL_BGR, GL_UNSIGNED_BYTE, flipmat.ptr());
		//cv::imshow("test", frame);
		if (cvWaitKey(30) > 0) {
			isStop = true;
		}
		SDL_GL_SwapWindow(mainwindow);
	}
	SDL_Quit();
	capture.release();
	//system("pause");
	return 0;
}

int sdglInit() {
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		cout << "SDL_Init Error: " << SDL_GetError() << endl;
		return 1;
	}
	mainwindow = SDL_CreateWindow("cvgl", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (mainwindow == nullptr) {
		cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
		return 1;
	}
	maincontext = SDL_GL_CreateContext(mainwindow);
	if (maincontext == nullptr) {
		cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << endl;
		return 1;
	}
	/* This makes our buffer swap syncronized with the monitor's vertical refresh */
	SDL_GL_SetSwapInterval(1);
	var _r = glewInit();
	if (_r != GLEW_OK) {
		cout << "glewInit Error: " << endl;
		return 1;
	}
	return 0;
}

GLuint matToTexture(cv::Mat &mat, GLenum minFilter, GLenum magFilter, GLenum wrapFilter)
{
	// Generate a number for our textureID's unique handle
	GLuint textureID;
	glGenTextures(1, &textureID);

	// Bind to our texture handle
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Catch silly-mistake texture interpolation method for magnification
	if (magFilter == GL_LINEAR_MIPMAP_LINEAR ||
		magFilter == GL_LINEAR_MIPMAP_NEAREST ||
		magFilter == GL_NEAREST_MIPMAP_LINEAR ||
		magFilter == GL_NEAREST_MIPMAP_NEAREST)
	{
		cout << "You can't use MIPMAPs for magnification - setting filter to GL_LINEAR" << endl;
		magFilter = GL_LINEAR;
	}

	// Set texture interpolation methods for minification and magnification
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

	// Set texture clamping method
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapFilter);

	// Set incoming texture format to:
	// GL_BGR       for CV_CAP_OPENNI_BGR_IMAGE,
	// GL_LUMINANCE for CV_CAP_OPENNI_DISPARITY_MAP,
	// Work out other mappings as required ( there's a list in comments in main() )
	GLenum inputColourFormat = GL_BGR;
	if (mat.channels() == 1)
	{
		inputColourFormat = GL_LUMINANCE;
	}

	// Create the texture
	glTexImage2D(GL_TEXTURE_2D,     // Type of texture
		0,                 // Pyramid level (for mip-mapping) - 0 is the top level
		GL_RGB,            // Internal colour format to convert to
		mat.cols,          // Image width  i.e. 640 for Kinect in standard mode
		mat.rows,          // Image height i.e. 480 for Kinect in standard mode
		0,                 // Border width in pixels (can either be 1 or 0)
		inputColourFormat, // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
		GL_UNSIGNED_BYTE,  // Image data type
		mat.ptr());        // The actual image data itself

						   // If we're using mipmaps then generate them. Note: This requires OpenGL 3.0 or higher
	if (minFilter == GL_LINEAR_MIPMAP_LINEAR ||
		minFilter == GL_LINEAR_MIPMAP_NEAREST ||
		minFilter == GL_NEAREST_MIPMAP_LINEAR ||
		minFilter == GL_NEAREST_MIPMAP_NEAREST)
	{
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	return textureID;
}

void checkSDLError(int line)
{
#ifndef NDEBUG  
	const char *error = SDL_GetError();
	if (*error != '\0')
	{
		printf("SDL Error: %s\n", error);
		if (line != -1)
			printf(" + line: %i\n", line);
		SDL_ClearError();
	}
#endif  
}